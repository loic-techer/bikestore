<?php

use App\Http\Controllers\BrandsController;
use App\Http\Controllers\CategoryController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\LoginController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


//Route::group(['middleware' => ['is_connected']], function(){
    Route::get('/', [HomeController::class, 'index']);
//});

Route::get('/login', [LoginController::class, 'index']);
//Route::post('/login', [LoginController::class, 'login']);
//Route::get('/brands-json', [BrandsController::class, 'allJson']);

//Route::get('/brands', [LoginController::class, 'index']);
Route::get('/categories/create', [CategoryController::class, 'form']);
Route::post('/categories/create', [CategoryController::class, 'create']);
Route::get('/categories/{id}/edit', [CategoryController::class, 'formUpdate']);
Route::post('/categories/{id}/edit', [CategoryController::class, 'update']);
Route::get('/categories/{id}/delete', [CategoryController::class, 'delete']);
Route::get('/categories/json', [CategoryController::class, 'allJson']);
