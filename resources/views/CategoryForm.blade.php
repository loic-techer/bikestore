@extends('template')

@section('contenu')

<h4>CREATION d'une nouvelle catégorie</h4>
<form name="categorie" action="{{ !empty($category) ? url('/categories/'.$category->getId().'/edit') : url('/categories/create')}}" method="post">
    @csrf
    <div class="form-categorie">
        <input name="categorie" type="text" class="form-control form-control-user" id="categorie" placeholder="New categorie" value="{{ !empty($category) ? $category->getName()  : "" }}">
    </div>
    <br>
    <input class="btn btn-primary btn-user btn-block" type="submit" value="submit">
</form>

@stop
