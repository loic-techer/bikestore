<?php

namespace App\Http\Controllers;

use App\Http\Requests\CategorieRequest;
use App\Models\Manager\BrandsManagerInterface;
use App\Models\Manager\CategoryManagerInterface;
use App\Models\Models\Brands;
use App\Models\Models\Category;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\JsonResponse;

class CategoryController extends Controller
{
    public function index(){

    }

    public function form(){
        return view('CategoryForm');
    }

    public function formUpdate(CategoryManagerInterface $categoryManager, $id){
        $category = $categoryManager->getCategoryById($id);
        return view('CategoryForm')->with([
            "category" => $category
        ]);

    }

    public function create(CategorieRequest $request, CategoryManagerInterface $categoryManager){
        $cat = $request->input('categorie');
        
        $category = new Category();
        $category->setName($cat);

        $categoryManager->createCategory($category);

        //return view('test')->with(["catval"=> $cat]);
        return redirect('/');

    }

    public function update(CategorieRequest $request, CategoryManagerInterface $categoryManager, $id){
        $cat = new Category();
        $cat->setId($id);
        $cat->setName($request->input("categorie"));

        $categoryManager->updateCategory($cat);

        //return view('test')->with(["catval"=> $cat]);
        return redirect('/categories/' . $cat->getId() . '/edit');
    }


    public function delete(CategoryManagerInterface $categoryManager, $id){
        $categoryManager->deleteCategoryById($id);
        return redirect('/');
    }

    public function allJson(CategoryManagerInterface $categoryManager){
        $categories = $categoryManager->getAllCategories();
        return response()->json($categories);
    }
    
}
