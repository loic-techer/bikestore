<?php

namespace App\Http\Controllers;

use App\Exceptions\DaoException;
use App\Exceptions\UserNotFoundException;
use App\Models\Manager\BrandsManagerInterface;
use App\Models\Manager\CategoryManagerInterface;

use App\Models\Manager\ProductManagerInterface;
use App\Models\Manager\StockManagerInterface;


class HomeController extends Controller
{

    public function index(ProductManagerInterface $productManager, 
    BrandsManagerInterface $brandsManager, 
    StockManagerInterface $stockManager, 
    CategoryManagerInterface $categoryManager){

        //throw new DaoException();
        $allBrands = $brandsManager->getAllBrands();
        $allProducts = $productManager->getAllProducts();
        $allStock = $stockManager->getAllStocks();
        $allCategories = $categoryManager->getAllCategories();

        return view('home')->with([
            "brands" => $allBrands,
            "products" => $allProducts,
            "stocks" => $allStock,
            "categories" => $allCategories
        ]);
    }
}
