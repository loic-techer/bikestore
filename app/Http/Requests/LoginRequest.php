<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class LoginRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'username' => 'required|min:5|max:20|alpha',
            'email' => 'required|email',
            'test' => 'required|email',
        ];
    }

    public function messages(){
        return [
            'username.required' => 'bla',
            'username.min' => 'bla',
            'email.required' => 'blo',
            'username.max' => 'bla',
        ];
    }
}
