<?php

namespace App\Models\Manager;

use App\Models\Models\Category;

interface CategoryManagerInterface{
    public function getAllCategories();
    public function getCategoryById($id);
    public function createCategory(Category $category);
    public function updateCategory(Category $category);
    public function deleteCategoryById($id);

}