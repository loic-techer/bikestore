<?php

namespace App\Models\Manager;

interface ProductManagerInterface{
    public function getAllProducts();
    public function countProductWithCategoryId($id);
}