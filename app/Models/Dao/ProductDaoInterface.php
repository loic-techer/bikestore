<?php

namespace App\Models\Dao;

interface ProductDaoInterface{
    public function getAllProducts();
    public function countProductWithCategoryId($id);
    public function getProductById($id);
}