<?php

namespace App\Models\dao;

use App\Exceptions\DaoException;
use App\Models\Dao\BrandsDaoInterface;
use App\Models\Models\Brands;
use Exception;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class BrandsDaoImpl implements BrandsDaoInterface{

    public function getAllBrands(){

        $resultbdd = DB::select('exec dbo.get_all_brands');
        
        $allBrands = [];
        foreach($resultbdd as $i => $row){
            $brand = new Brands();
            $brand->setId($row->brand_id);
            $brand->setName($row->brand_name);

            array_push($allBrands, $brand);
        }

        return $allBrands;
    }

    public function getBrandById($id) : Brands{

        try{
            $bdd = DB::getPdo();
            $reponse = $bdd->query("SELECT * FROM production.brands WHERE brand_id='".$id."'");
            $resultbdd = $reponse->fetch();
    
            $brand = new Brands();
            $brand->setId($resultbdd['brand_id']);
            $brand->setName($resultbdd['brand_name']);
    
            return $brand;
        }catch(Exception $e){
            Log::error($e);
            //throw new DaoException();
        }
        
    }
}